# -*- coding: utf-8 -*-

def greeting(username):
    print("hello, %s", username)


if __name__ == "__main__":
    greeting("Kyungmun Ryu")
